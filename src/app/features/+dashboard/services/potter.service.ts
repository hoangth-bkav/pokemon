import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({providedIn: 'root'})
export class PotterService {

  public characters$: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private baseUrl = 'https://www.potterapi.com/v1';  // URL to web api
  private key = '$2a$10$D.aGHQiYoSkeMqmR6aKdCOZoPn6EcdlKDNV.gd9208ihqadPi/X4y';

  constructor(private http: HttpClient) {}

  /** GET characters from the server */
  getCharacters(query: string): Observable<any[]> {
    const url = `${this.baseUrl}/characters?key=${this.key}`;
    return this.http.get<any[]>(url)
      .pipe(
        map(characters => {
          const normalizedQuery = (query || '').toLowerCase();
          return normalizedQuery !== ''
            ? characters.filter(c => c.name.toLowerCase().includes(normalizedQuery))
            : characters;
        }),
        tap((characters) => {
          console.log('fetched characters');
          this.characters$.next(characters);
        }),
        catchError(this.handleError<any[]>('getHeroes', []))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
