import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './pages';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: DashboardComponent }
    ])
  ],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
