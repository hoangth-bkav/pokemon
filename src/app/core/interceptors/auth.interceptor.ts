import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggerService } from '../services';
import { AuthService } from '../services';

export const InterceptorSkipHeader = 'X-Skip-Interceptor';

// TODO: should not need this, always use skip header
const hasAuthorization = (req: HttpRequest<any>) => req.headers.keys().includes('Authorization');

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private _authSvc: AuthService, private _loggerSvc: LoggerService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.headers.has(InterceptorSkipHeader)) {
      this._loggerSvc.log('Skipping interceptor');
      const headers = req.headers.delete(InterceptorSkipHeader);

      return next.handle(req.clone({ headers }));
    } else {
      const token = this._authSvc.getAccessToken();
      this._loggerSvc.log(`Bearer ${token}`);
      let authReq = req.clone();

      if (token && !hasAuthorization(req)) {
        // Add token
        const authHeader = `Bearer ${token}`;
        authReq = req.clone({ setHeaders: { Authorization: authHeader } });
        this._loggerSvc.log(`New headers: ${authReq.headers.keys()}`);
      }

      return next.handle(authReq);
    }
  }
}
