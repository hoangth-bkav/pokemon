import { AuthService } from './auth.service';
import { LoggerService } from './logger.service';

export * from './auth.service';
export * from './logger.service';

export const AppServices = [
  AuthService,
  LoggerService
];
