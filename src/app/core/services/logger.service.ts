import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { NGXLogger } from 'ngx-logger';

const noop = (): any => undefined;

@Injectable({ providedIn: 'root' })
export class LoggerService {
  log: any;
  warn: any;
  debug: any;
  error: any;

  constructor(private logger: NGXLogger) {
    console.log('LogService ctor');
    this.setupLogs('core');
  }

  // rename log service
  // TODO: find a better way to inject name
  init(name: string) {
    this.setupLogs(name);
  }

  setupLogs(name: string) {
    console.log('LogService init', name);
    this.error = this.logger.error.bind(this.logger);
    if (!environment.production) {
      this.debug = this.logger.debug.bind(this.logger);
      this.log = this.logger.info.bind(this.logger);
      this.warn = this.logger.warn.bind(this.logger);
    } else {
      this.debug = noop;
      this.log = noop;
      this.warn = noop;
    }
  }
}
